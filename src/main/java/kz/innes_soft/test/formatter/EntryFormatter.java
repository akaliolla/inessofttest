package kz.innes_soft.test.formatter;

import kz.innes_soft.test.model.Entry;

import java.util.List;

public interface EntryFormatter {
    String format( String entryFormat, List<Entry> entries, String headerFormat, String ... header);
}
