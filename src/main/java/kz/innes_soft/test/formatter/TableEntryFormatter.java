package kz.innes_soft.test.formatter;

import kz.innes_soft.test.model.Entry;

import java.util.List;

public class TableEntryFormatter implements EntryFormatter{

    @Override
    public String format(String entryFormat, List<Entry> entries,  String headerFormat, String... header) {
        StringBuilder builder = new StringBuilder();
        builder.append(starLine());
        builder.append(String.format(headerFormat, header));
        builder.append(starLine());
        for (Entry entry : entries ) {
            builder.append(getFormatEntry(entry, entryFormat));
            builder.append(line());
        }
        return builder.toString();
    }

    private String getFormatEntry(Entry entry, String format) {
        return String.format(format, entry.getId(), entry.getType().getTitle(), entry.getName(), entry.getPublishingDate(), entry.getPageCount(), entry.getPublishingHouse());
    }

    private String line() {
        StringBuilder line = new StringBuilder("\n");
        for (int i = 0; i < 3; i++){
            line.append("----------------------------------------------------");
        }
        return line.append("\n").toString();
    }

    private String starLine() {
        StringBuilder line = new StringBuilder("\n");
        for (int i = 0; i < 3; i++){
            line.append("****************************************************");
        }
        return line.append("\n").toString();
    }
}
