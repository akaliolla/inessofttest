package kz.innes_soft.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;

public class Mian {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        ConsoleManager consoleManager = context.getBean(ConsoleManager.class);
        DataSource dataSource = context.getBean(DataSource.class);
        DataBase dataBase = new DataBase();
        dataBase.init(dataSource);
        dataBase.createTables();
        dataBase.insertData();
        consoleManager.start();
    }
}
