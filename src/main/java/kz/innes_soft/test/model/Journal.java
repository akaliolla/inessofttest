package kz.innes_soft.test.model;

public class Journal extends BaseEntry{
    private String articles;

    public String getArticles() {
        return articles;
    }

    public void setArticles(String articles) {
        this.articles = articles;
    }
}