package kz.innes_soft.test.model;

/**
 *
 * @author Ayan Kaliolla
 * @version 1.0
 *
 **/

 
public class Entry extends BaseEntry{
    private EntryType type;

    public EntryType getType() {
        return type;
    }

    public void setType(EntryType type) {
        this.type = type;
    }
}
