package kz.innes_soft.test.model;

/**
 *
 * @author Ayan Kaliolla
 * @version 1.0
 *
 **/

 
public class Book extends Brochure{
    private String author;
    private long genreId;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long genreId) {
        this.genreId = genreId;
    }
}
