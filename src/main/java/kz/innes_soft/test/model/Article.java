package kz.innes_soft.test.model;


/**
 *
 * @author Ayan Kaliolla
 * @version 1.0
 *
 **/

public class Article {
    private long id;
    private long journalId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getJournalId() {
        return journalId;
    }

    public void setJournalId(long journalId) {
        this.journalId = journalId;
    }
}
