package kz.innes_soft.test.model;

import java.util.Date;

/**
 * BaseEntry - abstract class for entry
 *
 * @author Ayan Kaliolla
 * @version 1.0
 *
 **/

public abstract class BaseEntry {
    private long id;
    private String name;
    private String publishingDate;
    private int pageCount;
    private String publishingHouse;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(String publishingDate) {
        this.publishingDate = publishingDate;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }
}