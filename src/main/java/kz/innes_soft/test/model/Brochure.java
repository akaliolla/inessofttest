package kz.innes_soft.test.model;

/**
 *
 * @author Ayan Kaliolla
 * @version 1.0
 *
 **/
 

public class Brochure extends BaseEntry{
    private String summary;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
