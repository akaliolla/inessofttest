package kz.innes_soft.test;

public class Constants {
    public static final String ENTRY_FORMAT = "|%8d | %10s | %35s | %30s | %5d | %50s |";
    public static final String ENTRY_HEADER_FORMAT = "|%8s | %10s | %35s | %30s | %5s | %50s |";
}
