package kz.innes_soft.test;

import kz.innes_soft.test.dao.BeanDAO;
import kz.innes_soft.test.formatter.EntryFormatter;
import kz.innes_soft.test.model.*;
import kz.innes_soft.test.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;


import java.util.*;

import static kz.innes_soft.test.Commands.*;
import static kz.innes_soft.test.Constants.ENTRY_FORMAT;
import static kz.innes_soft.test.Constants.ENTRY_HEADER_FORMAT;
import static kz.innes_soft.test.dao.impl.BookDAOImpl.COLUMN_GENRE;
import static kz.innes_soft.test.dao.impl.JournalDAOImpl.COLUMN_ARTICLES;

public class ConsoleManager {
    @Autowired
    private ApplicationContext appContext;
    @Autowired
    private EntryService service;
    @Autowired
    private EntryFormatter formatter;

    private Scanner scanner;

    public void start() {
        scanner = new Scanner(System.in);
        System.out.println("Welcome !!!");
        while (true) {
            System.out.print(Commands.getAllCommandsWithTitle());
            int command = getCommand(scanner);
            if (command == COMMAND_EXIT) {
                break;
            } else {
                execute(command);
            }
        }
    }

    private void execute(int command) {
        switch (command) {
            case COMMAND_UNKNOWN: {
                System.out.println(COMMAND_UNKNOWN_TITLE);
                System.out.println("\n\n\n");
                break;
            }
            case COMMAND_GET_ALL: {
                List<Entry> entries = service.getAll();
                System.out.println(formatter.format(ENTRY_FORMAT, entries, ENTRY_HEADER_FORMAT, getHeader()));
                System.out.println("\n\n\n");
                break;
            }
            case COMMAND_ADD: {
                EnterNewEntry(scanner);
                break;
            }
            case COMMAND_GET_ONE: {
                System.out.println("Введите идентификатор издания\n");
                int id = scanner.nextInt();
                EntryType entryType = service.getEntryTypeById(id);
                if (entryType != null) {
                    BeanDAO beanDAO = (BeanDAO) appContext.getBean(entryType.getBeanName());
                    List<Map.Entry<String, String>> data = beanDAO.getById(id);
                    show(data);
                    System.out.println("\n\n\n");
                } else {
                    System.out.println("Ничего не найден по идентификатору : " + id);
                    System.out.println("\n\n\n");
                }
                break;
            }
            case COMMAND_DELETE_ONE: {
                System.out.println("Введите идентификатор издания\n");
                long id = scanner.nextInt();
                EntryType entryType = service.getEntryTypeById(id);
                if (entryType != null) {
                    BeanDAO beanDAO = (BeanDAO) appContext.getBean(entryType.getBeanName());
                    if (beanDAO.deleteById(id)) {
                        System.out.println("Запись по идентификатору : " + id + " был удален");
                    } else {
                        System.out.println("Запись по идентификатору : " + id + " не был удален");
                    }
                    System.out.println("\n\n\n");
                } else {
                    System.out.println("Ничего не найдено по идентификатору : " + id);
                    System.out.println("\n\n\n");
                }
                break;
            }
        }
    }

    private void EnterNewEntry(Scanner scanner) {
        List<EntryType> entryTypes = service.getAllEntryType();
        showEntryType(entryTypes);
        System.out.println("Введите тип записи");
        long id = scanner.nextInt();
        EntryType entryType = checkEntryType(id, entryTypes);
        if (entryType != null) {
            addNewEntry(entryType);
        } else {
            System.out.println("Тип по идентификатору " + id + "не найден");
        }
    }

    private void addNewEntry(EntryType entryType) {
        BeanDAO beanDAO = (BeanDAO) appContext.getBean(entryType.getBeanName());
        List<String> allColumnName = beanDAO.getAllColumnName();
        List<Map.Entry<String, String>> entries = new LinkedList<>();
        for (String colName : allColumnName) {
            if ("Тип".equals(colName)) {
                entries.add(new AbstractMap.SimpleEntry<String, String>(colName, String.valueOf(entryType.getId())));
            }else if (COLUMN_GENRE.equals(colName)) {
                List<Genre> genres = service.getAllGenre();
                showGenre(genres);
                String next = scanner.next();
                entries.add(new AbstractMap.SimpleEntry<String, String>(colName, next));
            }else if (COLUMN_ARTICLES.equals(colName)){
                System.out.println("Введите статби через запятой :");
                String next = scanner.next();
                entries.add(new AbstractMap.SimpleEntry<String, String>(colName, next));
            }
        }
        beanDAO.insert(entries);
    }

    private void showGenre(List<Genre> genres) {
        System.out.println("Выберите жанр: ");
        for (Genre genre : genres) {
            System.out.println(genre.getId() + " - " + genre.getTitle());
        }
    }

    private void showEntryType(List<EntryType> entryTypes) {
        for (EntryType entryType : entryTypes) {
            System.out.println(entryType.getId() + " - " + entryType.getTitle());
        }
    }

    private EntryType checkEntryType(long id, List<EntryType> entryTypes) {
        EntryType entryType = null;
        for (EntryType entryTypeTmp : entryTypes) {
            if (id == entryTypeTmp.getId()) {
                entryType = entryTypeTmp;
                break;
            }
        }
        return entryType;
    }

    private void show(List<Map.Entry<String, String>> data) {
        for (Map.Entry<String, String> entry : data) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }


    public int getCommand(Scanner scanner) {
        if (scanner.hasNext()) {
            int tmp = scanner.nextInt();
            if (tmp >= COMMAND_MIN_VALUE && tmp <= COMMAND_MAX_VALUE) {
                return tmp;
            } else {
                return -1;
            }
        }
        return -1;
    }

    public String[] getHeader() {
        return new String[]{"id", "Тип", "Название", "Дата издательства", "стр.", "Издательство"};
    }
}
