package kz.innes_soft.test;

public class Commands {
    public static final int COMMAND_MIN_VALUE = 1;
    public static final int COMMAND_MAX_VALUE = 5;
    public static final int COMMAND_UNKNOWN = -1;
    public static final int COMMAND_GET_ALL = 1;
    public static final int COMMAND_ADD = 2;
    public static final int COMMAND_GET_ONE = 3;
    public static final int COMMAND_DELETE_ONE = 4;
    public static final int COMMAND_EXIT = 5;

    static final String COMMAND_UNKNOWN_TITLE = "Команда не найдена введите еще раз";
    private static final String COMMAND_GET_ALL_TITLE = "Просмотр зарегистрированных изданий в фонде";
    private static final String COMMAND_ADD_TITLE = "Добавление нового издания в фонд";
    private static final String COMMAND_GET_ONE_TITLE = "Просмотр информации выбронного издания";
    private static final String COMMAND_DELETE_ONE_TITLE = "Удаление выбранного издания";
    private static final String COMMAND_EXIT_TITLE = "Выход";

    public static String getAllCommandsWithTitle(){
        StringBuilder commands = new StringBuilder();
        commands.append(COMMAND_GET_ALL + " - " + COMMAND_GET_ALL_TITLE + "\n");
        commands.append(COMMAND_ADD + " - " + COMMAND_ADD_TITLE + "\n");
        commands.append(COMMAND_GET_ONE + " - " + COMMAND_GET_ONE_TITLE + "\n");
        commands.append(COMMAND_DELETE_ONE + " - " + COMMAND_DELETE_ONE_TITLE + "\n");
        commands.append(COMMAND_EXIT + " - " + COMMAND_EXIT_TITLE + "\n");
        return commands.toString();
    }

}
