package kz.innes_soft.test.dao;

import kz.innes_soft.test.model.Entry;

import java.util.List;

public interface EntryDAO {

    List<Entry> getAll();
}
