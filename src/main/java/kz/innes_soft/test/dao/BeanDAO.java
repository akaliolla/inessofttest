package kz.innes_soft.test.dao;

import kz.innes_soft.test.model.BaseEntry;
import kz.innes_soft.test.model.Brochure;
import kz.innes_soft.test.model.Entry;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface BeanDAO {
    String COLUMN_ID = "id";
    String COLUMN_NAME = "Название";
    String COLUMN_DATA_PUBLISH = "Дата издательства";
    String COLUMN_PAGE_COUNT = "стр.";
    String COLUMN_PUBLISHING_HOUSE = "Издательство";


    default void setData(Map.Entry<String, String> entryMap, BaseEntry entry) {
        switch (entryMap.getKey()){
            case COLUMN_ID :
                entry.setId(Long.parseLong(entryMap.getValue()));
                break;
            case COLUMN_NAME :
                entry.setName(entryMap.getValue());
                break;
            case COLUMN_DATA_PUBLISH :
                entry.setPublishingDate(entryMap.getValue());
                break;
            case COLUMN_PAGE_COUNT :
                entry.setPageCount(Integer.parseInt(entryMap.getValue()));
                break;
            case COLUMN_PUBLISHING_HOUSE :
                entry.setPublishingHouse(entryMap.getValue());
                break;
        }
    }

    List<Map.Entry<String, String>> getById(long id);
    boolean deleteById(long id);
    long insert(List<Map.Entry<String, String>> bean);
    List<String> getAllColumnName();
}