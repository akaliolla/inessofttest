package kz.innes_soft.test.dao;

import kz.innes_soft.test.model.Genre;

import java.util.List;

public interface GenreDAO {

    List<Genre> getAll();
}
