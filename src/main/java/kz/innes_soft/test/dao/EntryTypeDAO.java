package kz.innes_soft.test.dao;

import kz.innes_soft.test.model.EntryType;

import java.util.List;

public interface EntryTypeDAO {
    List<EntryType> getAll();

    EntryType getById(long id);
}
