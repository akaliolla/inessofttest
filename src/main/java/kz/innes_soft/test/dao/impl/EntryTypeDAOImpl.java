package kz.innes_soft.test.dao.impl;

import kz.innes_soft.test.dao.EntryTypeDAO;
import kz.innes_soft.test.model.EntryType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class EntryTypeDAOImpl implements EntryTypeDAO{
    private static final String SELECT_ENTRY_TYPE_ALL = "SELECT * FROM entry_type;";
    private static final String SELECT_ENTRY_TYPE_BY_ID = "SELECT t.id, t.title, t.bean_name FROM entries as e LEFT JOIN entry_type as t ON e.type_id = t.id WHERE e.id = ?;";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<EntryType> getAll() {
        List<EntryType> result = new LinkedList<>();
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement(); ResultSet resultSet = statement.executeQuery(SELECT_ENTRY_TYPE_ALL)) {
            EntryType entryType;
            while (resultSet.next()){
                entryType = new EntryType();
                entryType.setId(resultSet.getLong("id"));
                entryType.setTitle(resultSet.getString("title"));
                entryType.setBeanName(resultSet.getString("bean_name"));
                result.add(entryType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public EntryType getById(long id) {
        EntryType entryType = null;
        ResultSet resultSet = null;
        try (Connection conn = dataSource.getConnection(); PreparedStatement statement = conn.prepareStatement(SELECT_ENTRY_TYPE_BY_ID)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                entryType = new EntryType();
                entryType.setId(resultSet.getLong("id"));
                entryType.setTitle(resultSet.getString("title"));
                entryType.setBeanName(resultSet.getString("bean_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entryType;
    }
}
