package kz.innes_soft.test.dao.impl;

import kz.innes_soft.test.dao.GenreDAO;
import kz.innes_soft.test.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class GenreDAOImpl implements GenreDAO {

    private static final String SELECT_ALL_GENERE = "SELECT * FROM genre;";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Genre> getAll() {
        List<Genre> result = null;
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement(); ResultSet resultSet = statement.executeQuery(SELECT_ALL_GENERE);) {
            result = new LinkedList<>();
            Genre genre;
            while (resultSet.next()) {
                genre = new Genre();
                genre.setId(resultSet.getInt("id"));
                genre.setTitle(resultSet.getString("title"));
                result.add(genre);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
