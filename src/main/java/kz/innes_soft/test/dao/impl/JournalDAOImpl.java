package kz.innes_soft.test.dao.impl;

import kz.innes_soft.test.dao.BeanDAO;
import kz.innes_soft.test.model.BaseEntry;
import kz.innes_soft.test.model.Brochure;
import kz.innes_soft.test.model.Journal;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

public class JournalDAOImpl implements BeanDAO {
    private static final int JOURNAL_TYPE_ID = 2;

    public static final String COLUMN_ARTICLES = "Статьи";

    private static final String SELECT_JOURNAL_BY_ID = "SELECT * FROM entries as e " +
            "LEFT JOIN entry_type as t ON e.type_id = t.id WHERE e.id = ?;";
    private static final String SELECT_ARTICLES_BY_ID = "SELECT title FROM article WHERE journal_id = ?;";

    private static final String DELETE_ENTRY_BY_ID = "DELETE FROM entries WHERE id = ";
    private static final String DELETE_ARTICLES_BY_ID = "DELETE FROM article WHERE journal_id = ";;

    private static final String INSERT_ENTRY = "INSERT INTO entries";
    private static final String INSERT_ARTICLES = "INSERT INTO article";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Map.Entry<String, String>> getById(long id) {
        List<Map.Entry<String, String>> result = null;
        try (Connection conn = dataSource.getConnection(); PreparedStatement statement = conn.prepareStatement(SELECT_JOURNAL_BY_ID)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new LinkedList<>();
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_ID, String.valueOf(resultSet.getInt("id"))));
                result.add(new AbstractMap.SimpleEntry<String, String>("Тип", resultSet.getString("title")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_NAME, resultSet.getString("name")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_DATA_PUBLISH, resultSet.getString("data_publish")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_PAGE_COUNT, String.valueOf(resultSet.getInt("page_count"))));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_PUBLISHING_HOUSE, resultSet.getString("publishing_house")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_ARTICLES, getArticles(id)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public boolean deleteById(long id) {
        boolean result = false;
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            conn.setAutoCommit(false);
            try {
                statement.execute(DELETE_ENTRY_BY_ID + id);
                statement.execute(DELETE_ARTICLES_BY_ID + id);
            } catch (Exception e) {
                e.printStackTrace();
                conn.rollback();
            }
            conn.commit();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            conn.setAutoCommit(false);
            try {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM article WHERE journal_id = " + id);
                if (resultSet.next()){
                    System.out.println(resultSet.getLong("id"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                conn.rollback();
            }
            conn.commit();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public long insert(List<Map.Entry<String, String>> entries) {
        Journal journal = parseBrochure(entries);
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            conn.setAutoCommit(false);
            try {
                statement.execute(INSERT_ENTRY + " VALUES(" + journal.getId() +", " + JOURNAL_TYPE_ID +", '" + journal.getName() + "', " +
                        "'" + journal.getPublishingDate() +"', " + journal.getPageCount() +", '" + journal.getPublishingHouse() +"');");
                for (String str : journal.getArticles().split(",")) {
                    statement.execute(INSERT_ARTICLES + "(journal_id, title) VALUES(" + journal.getId() + ", '" + str + "');");
                }
            } catch (Exception e) {
                e.printStackTrace();
                conn.rollback();
            }
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return journal.getId();
    }

    private Journal parseBrochure(List<Map.Entry<String, String>> entries) {
        Journal journal = new Journal();
        for (Map.Entry<String, String> entry : entries){
            switch (entry.getKey()) {
                case COLUMN_ARTICLES:
                    journal.setArticles(entry.getValue());
                default:
                    setData(entry, (BaseEntry) journal);
            }
        }
        return journal;
    }


    @Override
    public List<String> getAllColumnName() {
        List<String> columnNames = new LinkedList<>();
        columnNames.add(COLUMN_ID);
        columnNames.add(COLUMN_NAME);
        columnNames.add(COLUMN_DATA_PUBLISH);
        columnNames.add(COLUMN_PAGE_COUNT);
        columnNames.add(COLUMN_PUBLISHING_HOUSE);
        columnNames.add(COLUMN_ARTICLES);
        return columnNames;
    }

    public String getArticles(long id) {
        StringBuilder builder = new StringBuilder();
        try (Connection conn = dataSource.getConnection(); PreparedStatement statement = conn.prepareStatement(SELECT_ARTICLES_BY_ID)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                builder.append(resultSet.getString("title") + ",");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
