package kz.innes_soft.test.dao.impl;

import kz.innes_soft.test.dao.EntryDAO;
import kz.innes_soft.test.model.Entry;
import kz.innes_soft.test.model.EntryType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class EntryDAOImpl implements EntryDAO{
    private static final String SELECT_ALL_ENTRIES = "SELECT e.id, e.type_id, e.name,e.data_publish, e.page_count, e.publishing_house, t.title FROM entries as e, entry_type as t WHERE e.type_id = t.id;";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Entry> getAll() {
        List<Entry> entries = new LinkedList<>();
        Entry entry;
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()){
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_ENTRIES);
            while (resultSet.next()){
                entry = new Entry();
                entry.setId(resultSet.getInt("id"));
                EntryType entryType = new EntryType();
                entryType.setId(resultSet.getInt("type_id"));
                entryType.setTitle(resultSet.getString("title"));
                entry.setType(entryType);
                entry.setName(resultSet.getString("name"));
                entry.setPublishingDate(resultSet.getString("data_publish"));
                entry.setPageCount(resultSet.getInt("page_count"));
                entry.setPublishingHouse(resultSet.getString("publishing_house"));
                entries.add(entry);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return entries;
    }
}
