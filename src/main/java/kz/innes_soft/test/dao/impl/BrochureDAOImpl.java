package kz.innes_soft.test.dao.impl;

import kz.innes_soft.test.dao.BeanDAO;
import kz.innes_soft.test.model.BaseEntry;
import kz.innes_soft.test.model.Brochure;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class BrochureDAOImpl implements BeanDAO {
    private static final int BROCHURE_TYPE_ID = 3;
    private static final String SELECT_BROCHURE_BY_ID = "SELECT e.id, t.title, e.name, e.data_publish, e.page_count, " +
            "e.publishing_house, b.summary" +
            " FROM entries as e " +
            "LEFT JOIN entry_type as t ON e.type_id = t.id " +
            "LEFT JOIN brochure as b ON e.id = b.id WHERE e.id = ?;";
    private static final String DELETE_ENTRY_BY_ID = "DELETE FROM entries WHERE id = ";
    private static final String DELETE_BROCHURE_BY_ID = "DELETE FROM brochure WHERE id = ";
    private static final String INSERT_ENTRY = "INSERT INTO entries";
    private static final String INSERT_BROCHURE = "INSERT INTO brochure";


    public static final String COLUMN_SUMMARY = "Краткое содержание";


    @Autowired
    private DataSource dataSource;

    //    private static final String INSERT_ONE_ENTRY_BROCHURE = "DELETE * FROM brochure WHERE id = ?";
//    private static final String INSERT_ONE_ENTRY_ARTICLE = "DELETE * FROM article WHERE id = ?";
    @Override
    public List<Map.Entry<String, String>> getById(long id) {
        List<Map.Entry<String, String>> result = null;
        try (Connection conn = dataSource.getConnection(); PreparedStatement statement = conn.prepareStatement(SELECT_BROCHURE_BY_ID)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = new LinkedList<>();
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_ID, String.valueOf(resultSet.getInt("id"))));
                result.add(new AbstractMap.SimpleEntry<String, String>("Тип", resultSet.getString("title")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_NAME, resultSet.getString("name")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_DATA_PUBLISH, resultSet.getString("data_publish")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_PAGE_COUNT, String.valueOf(resultSet.getInt("page_count"))));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_PUBLISHING_HOUSE, resultSet.getString("publishing_house")));
                result.add(new AbstractMap.SimpleEntry<String, String>(COLUMN_SUMMARY, resultSet.getString("summary")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteById(long id) {
        boolean result = false;
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            conn.setAutoCommit(false);
            try {
                statement.execute(DELETE_ENTRY_BY_ID + id);
                statement.execute(DELETE_BROCHURE_BY_ID + id);
            } catch (Exception e) {
                conn.rollback();
            }
            conn.commit();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public long insert(List<Map.Entry<String, String>> entries) {
        Brochure brochure = parseBrochure(entries);
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            conn.setAutoCommit(false);
            try {
                statement.execute(INSERT_ENTRY + " VALUES(" + brochure.getId() +", " + BROCHURE_TYPE_ID +", '" + brochure.getName() + "'," +
                        "'" + brochure.getPublishingDate() +"' , " + brochure.getPageCount() +", '" + brochure.getPublishingHouse() +"');");
                statement.execute(INSERT_BROCHURE + " VALUES(" + brochure.getId() +", '" + brochure.getSummary() +"');");
            } catch (Exception e) {
                e.printStackTrace();
                conn.rollback();
            }
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return brochure.getId();
    }

    private Brochure parseBrochure(List<Map.Entry<String, String>> entries) {
        Brochure brochure = new Brochure();
        for (Map.Entry<String, String> entry : entries){
            switch (entry.getKey()) {
                case COLUMN_SUMMARY:
                    brochure.setSummary(entry.getValue());
                    break;
                default:
                    setData(entry, (BaseEntry) brochure);
            }
        }
        return brochure;
    }


    @Override
    public List<String> getAllColumnName() {
        List<String> columns = new LinkedList<>();
        columns.add(COLUMN_ID);
        columns.add(COLUMN_NAME);
        columns.add(COLUMN_DATA_PUBLISH);
        columns.add(COLUMN_PAGE_COUNT);
        columns.add(COLUMN_PUBLISHING_HOUSE);
        columns.add(COLUMN_SUMMARY);
        return columns;
    }
}
