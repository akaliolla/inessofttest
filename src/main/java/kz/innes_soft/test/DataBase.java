package kz.innes_soft.test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataBase {
    private static final String CREATE_ENTRIES = "CREATE TABLE entries (\n" +
            "  id  INTEGER PRIMARY KEY,\n" +
            "  type_id INTEGER,\n" +
            "  name  VARCHAR(50),\n" +
            "  data_publish  VARCHAR(50),\n" +
            "  page_count  INTEGER,\n" +
            "  publishing_house  VARCHAR(50)\n" +
            ");";
    private static final String CREATE_ENTRY_TYPE = "CREATE TABLE entry_type (\n" +
            "  id         INTEGER PRIMARY KEY,\n" +
            "  bean_name VARCHAR(30),\n" +
            "  title  VARCHAR(50)\n" +
            ");";
    private static final String CREATE_ARTICLE = "CREATE TABLE article (\n" +
            "  id         INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
            "  journal_id INTEGER,\n" +
            "  title  VARCHAR(50)\n" +
            ");";
    private static final String CREATE_BOOK = "CREATE TABLE book (\n" +
            "  id         INTEGER PRIMARY KEY,\n" +
            "  author VARCHAR(50),\n" +
            "  genre_id  INTEGER ,\n" +
            "  summary VARCHAR(50)\n" +
            ");";
    private static final String CREATE_GENRE = "CREATE TABLE genre (\n" +
            "  id         INTEGER PRIMARY KEY,\n" +
            "  title VARCHAR(50)\n" +
            ");";

    private static final String CREATE_BROCHURE = "CREATE TABLE brochure (\n" +
            "  id         INTEGER PRIMARY KEY,\n" +
            "  summary VARCHAR(50)\n" +
            ");";


    private static final String INSERT_ENTRY_TYPE_BOOK = "INSERT INTO entry_type VALUES (1, 'book', 'книга');";
    private static final String INSERT_ENTRY_TYPE_JOURNAL = "INSERT INTO entry_type VALUES (2, 'journal', 'журнал');";
    private static final String INSERT_ENTRY_TYPE_BROCHURE = "INSERT INTO entry_type VALUES (3, 'brochure', 'брошюра');";
    private static final String INSERT_GENRE_1 = "INSERT INTO genre VALUES (1, 'роман');";
    private static final String INSERT_GENRE_2 = "INSERT INTO genre VALUES (2, 'детектив');";
    private static final String INSERT_GENRE_3 = "INSERT INTO genre VALUES (3, 'лирика');";
    private static final String INSERT_GENRE_4 = "INSERT INTO genre VALUES (4, 'гангстерская драма');";
    private static final String INSERT_ENTRIES_BOOK_1 = "INSERT INTO entries VALUES ( 1, 1, 'Крестный отец', '1969 г', '388', 'г. Караганда');";
    private static final String INSERT_ENTRIES_BOOK_2 = "INSERT INTO entries VALUES ( 2, 1, 'Последний дон', '1996 г', '420', 'г. Караганда');";
    private static final String INSERT_ENTRIES_BROCHURE_1 = "INSERT INTO entries VALUES ( 3, 3, 'Тест брошюра', '2006 г', '8', 'г. Караганда');";
    private static final String INSERT_BOOK_1 = "INSERT INTO book VALUES (1, 'Марио Пьюзо', 1, 'Криминальная сага, повествующая ...');";
    private static final String INSERT_BOOK_2 = "INSERT INTO book VALUES (2, 'Марио Пьюзо', 1, 'Роман «Последний Дон» повествует ...');";
    private static final String INSERT_BROCHURE_1 = "INSERT INTO brochure VALUES (3, 'брошюра краткое содержание' );";

    private DataSource dataSource;

    public void init(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void createTables() {
        try (Connection conn = this.dataSource.getConnection(); Statement statement = conn.createStatement()) {
            statement.execute(CREATE_ENTRIES);
            statement.execute(CREATE_ENTRY_TYPE);
            statement.execute(CREATE_ARTICLE);
            statement.execute(CREATE_BOOK);
            statement.execute(CREATE_GENRE);
            statement.execute(CREATE_BROCHURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertData() {
        try (Connection conn = this.dataSource.getConnection(); Statement statement = conn.createStatement()) {
            statement.execute(INSERT_ENTRY_TYPE_BOOK);
            statement.execute(INSERT_ENTRY_TYPE_JOURNAL);
            statement.execute(INSERT_ENTRY_TYPE_BROCHURE);
            statement.execute(INSERT_GENRE_1);
            statement.execute(INSERT_GENRE_2);
            statement.execute(INSERT_GENRE_3);
            statement.execute(INSERT_GENRE_4);
            statement.execute(INSERT_ENTRIES_BOOK_1);
            statement.execute(INSERT_ENTRIES_BOOK_2);
            statement.execute(INSERT_ENTRIES_BROCHURE_1);
            statement.execute(INSERT_BOOK_1);
            statement.execute(INSERT_BOOK_2);
            statement.execute(INSERT_BROCHURE_1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
