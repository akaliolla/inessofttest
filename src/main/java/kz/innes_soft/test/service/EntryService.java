package kz.innes_soft.test.service;

import kz.innes_soft.test.model.Entry;
import kz.innes_soft.test.model.EntryType;
import kz.innes_soft.test.model.Genre;

import java.util.List;

public interface EntryService {
    List<Entry> getAll();
    EntryType getEntryTypeById(long id);
    List<EntryType> getAllEntryType();
    List<Genre> getAllGenre();
}
