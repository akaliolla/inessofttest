package kz.innes_soft.test.service;

import kz.innes_soft.test.dao.EntryDAO;
import kz.innes_soft.test.dao.EntryTypeDAO;
import kz.innes_soft.test.dao.GenreDAO;
import kz.innes_soft.test.model.Entry;
import kz.innes_soft.test.model.EntryType;
import kz.innes_soft.test.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;

import java.util.List;

public class EntryServiceImpl implements EntryService {
    @Autowired
    private EntryDAO entryDAO;
    @Autowired
    private GenreDAO genreDAO;
    @Autowired
    private EntryTypeDAO entryTypeDAO;

    @Override
    public List<Entry> getAll() {
        return entryDAO.getAll();
    }

    @Override
    public EntryType getEntryTypeById(long id) {
        return entryTypeDAO.getById(id);
    }

    @Override
    public List<EntryType> getAllEntryType() {
        return entryTypeDAO.getAll();
    }

    @Override
    public List<Genre> getAllGenre() {
        return genreDAO.getAll();
    }
}
